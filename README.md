# jECS - Health Management

This is an addition to the [jECS Framework](//tobsemarsteel/jecs) that allows Entities to have a health value (and thus to live and die).

## Setup

Checkout this repository or download the current release branch. Dependencies can be installed using with `maven install`.

## Usage

See the core framework Wiki on how to use the jECS Framework. Here is a quick example, for details see the Wiki.

Example entity using the `com.tmarsteel.jecs.system.health.HealthComponent`:

```
#!kotlin
open class Player(val name: String, e: EntitySystem) : BaseEntity(e) {

    private val healthComponent = HealthComponent(maxHealth = 100, currentHealthValue = 100)

    init {
        addComponent(healthComponent)
    }

    fun kill() {
        entitySystem.getComponentSystem(HealthSystem::class.java).queueHealthChange(
            this, -healthComponent.health, "call to Player.kill"
        )
    }

    override fun toString() = "Player $name"
}
```

Example of the game flow:


```
#!kotlin
fun main(args: Array<String>) {
    var es = EntitySystem()
    var healthSystem = HealthSystem()
    var player = Player("John", es)

    es.register(healthSystem)
    healthSystem.registerEntityHealthListener(object : EntityHealthListener {
        override fun onHealthChange(e: Entity, change: HealthChange) {
            println("Health of $e changed by ${change.difference} caused by: " +
                change.sources.joinToString("\n"))
        }

        override fun onDeath(e: Entity, change: HealthChange) {
            println("$e killed by: " +
                    change.sources.joinToString("\n"))
        }
    })

    player.kill()

    es.tick(1.toFloat())
    
    /* output:
    Health of Player John changed by -100 caused by: call to Player.kill
    Player John killed by: call to Player.kill
    */
}
```