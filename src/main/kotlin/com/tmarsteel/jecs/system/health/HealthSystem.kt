package com.tmarsteel.jecs.system.health

import com.tmarsteel.jecs.Entity
import com.tmarsteel.jecs.EntitySystem
import com.tmarsteel.jecs.system.ComponentSystem
import java.util.*

/**
 * Manages [HealthComponent]s.
 * @author Tobias Marstaller @gmail.com>
 */
class HealthSystem : ComponentSystem {
    /**
     * The queued changes of health for the next tick. Is reset after the tick.
     */
    private val nextTickChangeQueue = HashMap<Entity, HealthChange>()

    /**
     * The health change listeners
     */
    private val listeners = HashSet<EntityHealthListener>()

    /**
     * The modifiers registered per entity
     */
    private val modifiers = HashMap<Entity, MutableSet<(e: Entity, difference: Int, sources: Array<Any>) -> Int>>()

    /**
     * Is set to true when a tick begins and to false when it ends.
     */
    private var inTick = false

    /**
     * Registers the given modifier for the given [Entity]. When
     * [.queueHealthChange] is called with `e`, the given modifier will be
     * asked to apply any changes to the value.
     * @param e The entity to register the modifier with
     * @param modifier The modifier to register. Returns the new value of the change after modification. Example:
     * if {@code modifier(entity, -50, sources) == -40} the damage will be reduced by 10 hp.
     */
    fun registerModifier(e: Entity, modifier: (e: Entity, difference: Int, sources: Array<Any>) -> Int) {
        var modifierSet = this.modifiers[e]

        if (modifierSet == null) {
            modifierSet = HashSet()

            this.modifiers.put(e, modifierSet)
        }

        modifierSet.add(modifier)
    }

    /**
     * Unregisters the given modifier from the given [Entity].
     * @param e
     * @param modifier
     */
    fun unregisterModifier(e: Entity, modifier: (e: Entity, difference: Int, sources: Array<Any>) -> Int) = this.modifiers[e]?.remove(modifier)

    /**
     * Registers the given listener. Its [EntityHealthListener.onHealthChange] and
     * [EntityHealthListener.onDeath] methods will be called accordingly.
     * @param l The listener to register
     */
    fun registerEntityHealthListener(l: EntityHealthListener) = this.listeners.add(l)


    /**
     * Unregisters the previously registered listener. Its [EntityHealthListener.onHealthChange] and
     * [EntityHealthListener.onDeath] methods will not be called until it is registered
     * again by [.registerEntityHealthListener].
     * @param l The listener to unregister
     */
    fun unregisterEntityHealthListener(l: EntityHealthListener) = this.listeners.remove(l)

    /**
     * {@inheritdoc}
     */
    @Synchronized override fun tick(es: EntitySystem, tickDuration: Float) {
        inTick = true

        try {
            es.each({ entity: Entity ->
                val healthChange = nextTickChangeQueue[entity]

                if (healthChange != null)
                    applyHealthChange(entity, healthChange)
            }, HealthComponent::class.java)
        } finally {
            nextTickChangeQueue.clear()
            inTick = false
        }
    }

    /**
     * Applies the given change to the entity if it is not dead already and fires the listeners.
     * Used by [.tick] every tick and by [.queueHealthChange]
     * if it is called during a tick (see inTick).
     */
    private fun applyHealthChange(e: Entity, change: HealthChange) {
        val c = e.getComponent(HealthComponent::class.java)

        if (c.isDead) {
            // the entity is dead already... nothing to do
            return
        }

        // fire onHealthChange
        this.listeners.forEach { it.onHealthChange(e, change) }

        c.modifyHealth(change.difference)

        if (c.isDead) {
            // fire onDeath
            this.listeners.forEach { it.onDeath(e, change) }
        }
    }

    /**
     * Calculates the actual change in health by sending `change` through all modifiers.
     * registered with this system. The calculated change is then queued until the next tick; multiple changes are
     * summed up. If the total change is != 0 when the next tick occurs
     * [EntityHealthListener.onHealthChange] or [EntityHealthListener.onDeath]
     * will be called.
     * @param e The entity whichs health to change
     * *
     * @param change The amount of health to change (+ add health, -take health/damage)
     */
    fun queueHealthChange(e: Entity, change: Int, source: Any) {
        val modifierSet = this.modifiers[e]

        val actualChange: Int

        // if modifiers are registered for the entity, calculate the actual change
        if (modifierSet != null) {
            val sources = arrayOf(source)

            // the actual change is the sum of the modifications plus the original change.
            actualChange = change + modifierSet.sumBy({ it(e, change, sources) - change })
        } else {
            actualChange = change
        }

        if (inTick) {
            // tick is currently running; another system running concurrently most likely caused it. Apply it immediately
            this.applyHealthChange(e, HealthChange(actualChange, source))
        } else {
            // queue-sum the change for the next tick
            synchronized (nextTickChangeQueue) {
                if (nextTickChangeQueue.containsKey(e)) {
                    nextTickChangeQueue[e]!!.include(actualChange, source) // npe should never happen
                } else {
                    nextTickChangeQueue.put(e, HealthChange(actualChange, source))
                }
            }
        }
    }
}