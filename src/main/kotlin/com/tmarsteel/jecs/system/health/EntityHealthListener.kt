package com.tmarsteel.jecs.system.health

import com.tmarsteel.jecs.Entity

/**
 * Listener for health changes on [Entity]s.
 * @author Tobias Marstaller @
 */
interface EntityHealthListener {
    /**
     * Called just before the given entities [HealthComponent] health value changes by the value and due to the
     * reasons reported by `change`.
     * @param e      The affected entity
     * @param change The difference in health
     */
    fun onHealthChange(e: Entity, change: HealthChange)

    /**
     * Called when an entity dies (just after the health value of its [HealthComponent] drops below 0).
     * @param e The [Entity] that just died.
     * @param change The change that caused the death.
     */
    fun onDeath(e: Entity, change: HealthChange)
}
