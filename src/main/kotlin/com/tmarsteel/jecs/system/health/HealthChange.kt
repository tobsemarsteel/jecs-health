package com.tmarsteel.jecs.system.health

/**
 * Represents the causing of damage or healing to an entity. Since these effects always have origins/sources this
 * class was introduced to bind the change in health and the reasons together.
 * @author Tobias Marstaller
 */
class HealthChange
/**
 * @param difference The difference to the health value
 * @param sources The sources. Must not be empty since damage/health does not just come from nowhere.
 * @throws IllegalArgumentException If `sources` is null or has no elements.
 */
(difference: Int, vararg sources: Any) {

    var difference : Int = 0
        private set

    val sources : Array<Any> = arrayOf(*sources)
        get() {
            if (field.hashCode() != cachedSourcesHash) {
                field = sourcesList.toTypedArray()
                cachedSourcesHash = sourcesList.hashCode()
            }

            return field
        }

    private val sourcesList: MutableSet<Any> = mutableSetOf(*sources)

    /**
     * hashCode() of sourcesList when cachedSourcesArray was last generated
     */
    private var cachedSourcesHash = sourcesList.hashCode()

    init {
        if (sources.size == 0) {
            throw IllegalArgumentException("Cannot create a health change without at least one reason.")
        }

        this.difference = difference
    }

    /**
     * Adds the given diffenrence and source to this change.
     */
    fun include(difference: Int, source: Any) {
        this.difference += difference
        this.sourcesList.add(source)
    }
}