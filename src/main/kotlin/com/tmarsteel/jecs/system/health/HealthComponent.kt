package com.tmarsteel.jecs.system.health

import com.tmarsteel.jecs.component.Component
import java.util.*

/**
 * A [HealthComponent] is used by the [HealthSystem] to keep track of HPs of Entities. It makes entities
 * killable.
 * @author Tobias Marstaller
 */
class HealthComponent
/**
 * Creates a new HealthComponent with maxValue maximum value and initialHealthValue initial value.
 * @param maxValue The maximum health value for the new component
 * *
 * @param initialHealthValue The initial health value for the new component
 */
@JvmOverloads constructor(val maxHealth: Int = 100, private var currentHealthValue: Int = maxHealth) : Component {

    private val healthListeners = HashSet<HealthComponentStateListener>()

    val health : Int
        get() = currentHealthValue

    /**
     * Whether the health of this component is less than or equal to 0.
     */
    val isDead: Boolean
        get() = health <= 0

    /**
     * Alters the value of this component by diff.
     * **Important:** Calling this method does not trigger [EntityHealthListener]s registered on the [HealthSystem].
     * This will most likely lead to inconsistencies in your object tree and server/client sync. Use
     * [HealthSystem.queueHealthChange] instead.
     * @param diff The difference to apply to the health value
     * *
     * @throws ComponentDeadException If this components health is less than 1 before this operation is applied.
     */
    @Synchronized @Throws(ComponentDeadException::class)
    fun modifyHealth(diff: Int) {
        if (currentHealthValue <= 0) {
            throw ComponentDeadException()
        }

        if (-diff >= this.currentHealthValue) {
            // more health is to be subtracted than is still there => death
            // trigger onDeath
            this.healthListeners.forEach { it.onDeath(this) }

            this.currentHealthValue = 0 // don't drop below zero
        } else {
            // trigger onHealthUpdate
            this.healthListeners.forEach { it.onHealthUpdate(this, diff) }

            // apply the change, but limit at maxHealthValue
            this.currentHealthValue = Math.min(currentHealthValue + diff, maxHealth)
        }
    }

    /**
     * Assures the given [HealthComponentStateListener] is called whenever the health value of this component
     * changes.
     */
    fun registerStateListener(l: HealthComponentStateListener) {
        this.healthListeners.add(l)
    }

    /**
     * Assures the given [HealthComponentStateListener] is not called when the health value of this component
     * changes.
     */
    fun unregisterStateListener(l: HealthComponentStateListener) {
        this.healthListeners.remove(l)
    }

    /**
     * Thrown when an operation is to be performed on a [HealthComponent] that requires its health to be
     * greater than 0 when it is not.
     */
    class ComponentDeadException : RuntimeException()
}

/**
 * Health listener on entity components.
 * @author Tobias Marstaller
 */
interface HealthComponentStateListener {
    /**
     * Called when the health value of the given component is to be changed by the value `diff`.
     * **Important:** If the components health value drops below zero, [.onDeath] is called
     * instead of this method.
     * @param c    The component affected
     * *
     * @param diff The change
     */
    fun onHealthUpdate(c: HealthComponent, diff: Int)

    /**
     * Called when the health value of the given HealthComponent drops below 0.
     * @param c The health component affected
     */
    fun onDeath(c: HealthComponent)
}