package com.tmarsteel.jecs.system.health;

import com.tmarsteel.jecs.system.health.HealthChange;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author Tobias Marstaller <tobias.marstaller@gmail.com>
 */
public class HealthChangeTest
{
    @Test
    public void testInclude()
    {
        // SETUP
        Object sourceA = new Object();
        Object sourceB = new Object();

        HealthChange change = new HealthChange(10, sourceA);

        // ACT
        change.include(-15, sourceB);

        // ASSERT
        assertEquals(-5, change.getDifference());
        assertTrue("New source not merged", Arrays.asList(change.getSources()).contains(sourceB));
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructorShouldThrowOnNullSources()
    {
        new HealthChange(-10, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructorShouldThrowOnEmptySources()
    {
        new HealthChange(-10, new Object[]{});
    }
}