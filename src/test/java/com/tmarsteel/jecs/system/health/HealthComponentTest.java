package com.tmarsteel.jecs.system.health;

import com.tmarsteel.jecs.system.health.HealthComponent;
import com.tmarsteel.jecs.system.health.HealthComponentStateListener;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.internal.verification.VerificationModeFactory.times;

/**
 * @author Tobias Marstaller <tobias.marstaller@gmail.com>
 */
public class HealthComponentTest
{
    @Test
    public void constructorShouldApplyValues()
    {
        HealthComponent c = new HealthComponent(100, 50);

        assertEquals(100, c.getMaxHealth());
        assertEquals(50, c.getHealth());
    }

    @Test
    public void modifyShouldApplyChange()
    {
        HealthComponent c = new HealthComponent(100, 50);

        c.modifyHealth(+25);

        assertEquals(75, c.getHealth());
    }

    @Test
    public void modifyShouldCallOnHealthUpdate()
    {
        final HealthComponent c = new HealthComponent(100, 100);

        HealthComponentStateListener mockListener = mock(HealthComponentStateListener.class);

        c.registerStateListener(mockListener);

        c.modifyHealth(-10);

        verify(mockListener, times(1)).onHealthUpdate(c, -10);
        verify(mockListener, never()).onDeath(c);
    }

    @Test
    public void modifyShouldCallOnDeath()
    {
        final HealthComponent c = new HealthComponent(100, 10);

        HealthComponentStateListener mockListener = mock(HealthComponentStateListener.class);

        c.registerStateListener(mockListener);

        c.modifyHealth(-10);

        verify(mockListener, times(1)).onDeath(c);
        verify(mockListener, never()).onHealthUpdate(c, -10);
    }

    @Test
    public void tellValueShouldReturnZeroOnDeath()
    {
        HealthComponent c = new HealthComponent(100, 10);

        c.modifyHealth(-15);

        assertEquals(0, c.getHealth());
    }

    @Test
    public void modifyShouldNotRaiseAboveMaxValue()
    {
        HealthComponent c = new HealthComponent(100, 50);

        c.modifyHealth(+60);

        assertEquals(100, c.getHealth());
    }
}
