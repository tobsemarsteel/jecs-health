package com.tmarsteel.jecs.system.health

import com.tmarsteel.jecs.BaseEntity
import com.tmarsteel.jecs.Entity
import com.tmarsteel.jecs.EntitySystem
import org.junit.Before
import org.junit.Test

import java.util.concurrent.atomic.AtomicBoolean

/**
 * @author Tobias Marstaller @gmail.com>
 */
class HealthSystemTest {
    private var entitySystem: EntitySystem? = null
    private var healthSystem: HealthSystem? = null
    private var entity: Entity? = null
    private var healthComponent: HealthComponent? = null

    @Before
    fun setUp() {
        // component under test: HealthSystem
        healthSystem = HealthSystem()

        // Mocking this one is more complex than compensating the invisible test-dependency in case of an error
        healthComponent = HealthComponent(100, 50)

        entitySystem = EntitySystem()

        // mock the entity
        entity = BaseEntity(entitySystem!!)
        entity!!.addComponent(healthComponent!!)
    }

    @Test
    fun tickWithoutQueuedHealthChangeShouldLeaveHealthUnchanged() {
        // SETUP
        val healthBefore = healthComponent!!.health

        // ACT
        healthSystem!!.tick(entitySystem!!, .1f)

        // ASSERT
        assert(healthBefore.toLong() == healthComponent!!.health.toLong())
    }

    @Test
    fun queuedChangeShouldApplyOnTick() {
        // SETUP
        val healthBefore = healthComponent!!.health
        healthSystem!!.queueHealthChange(entity!!, -10, this)

        // ACT
        healthSystem!!.tick(entitySystem!!, .1f)

        // ASSERT
        assert((healthBefore - 10).toLong() == healthComponent!!.health.toLong())
    }

    @Test
    fun queuedChangeShouldNotBeAppliedTwice() {
        // SETUP
        val healthBefore = healthComponent!!.health
        healthSystem!!.queueHealthChange(entity!!, -10, this)

        // ACT
        healthSystem!!.tick(entitySystem!!, .1f)
        healthSystem!!.tick(entitySystem!!, .1f)

        // ASSERT
        assert((healthBefore - 10).toLong() == healthComponent!!.health.toLong())
    }

    @Test
    fun multipleQueuedChangesShouldBeAppliedCorrectly() {
        // SETUP
        val healthBefore = healthComponent!!.health
        healthSystem!!.queueHealthChange(entity!!, -10, this)
        healthSystem!!.queueHealthChange(entity!!, +3, this)

        // ACT
        healthSystem!!.tick(entitySystem!!, .1f)

        // ASSERT
        assert((healthBefore - 10 + 3).toLong() == healthComponent!!.health.toLong())
    }

    @Test
    fun healthModifierShouldBeInvoked() {
        // SETUP
        val wasCalled = AtomicBoolean(false)
        val modifier: (Entity, Int, Array<Any>) -> Int = { e, difference, sources ->
            if (e === entity) {
                wasCalled.set(true)
            }
            difference
        }

        healthSystem!!.registerModifier(entity!!, modifier)

        // ACT
        healthSystem!!.queueHealthChange(entity!!, -10, this)

        // VERIFY
        assert(wasCalled.get(),{"HealthChangeModifier was not invoked"})
    }

    @Test
    fun modifiedHealthChangeShouldBeApplied_SingleModifier() {
        // SETUP
        val healthBefore = healthComponent!!.health

        // the modifier returns -5 => the change of -10 should become -5
        healthSystem!!.registerModifier(entity!!, { entity, diff, sources -> -5 })

        // ACT
        healthSystem!!.queueHealthChange(entity!!, -10, this)
        healthSystem!!.tick(entitySystem!!, .1f)

        // ASSERT
        assert(healthBefore - 5 == healthComponent!!.health)
    }

    @Test
    fun modifiedHealthChangeShouldBeApplied_MultipleModifiers() {
        // SETUP
        val healthBefore = healthComponent!!.health

        // the first modifier returns -5 => the change of -10 should become -5
        healthSystem!!.registerModifier(entity!!, { entity, diff, sources -> -5 })

        // the second modifier returns +10 => the change of -10 should become 10
        healthSystem!!.registerModifier(entity!!, { entity, diff, sources -> +10 })

        // the two modifiers "conflict". the resolution algorithm mandates the changes of the change
        // to be summed up:
        // first modifier:   -5 - (-10) => change by +5
        // second modifier: +10 - (-10) => change by +20
        // -10 + 5 + 20 = +15

        // ACT
        healthSystem!!.queueHealthChange(entity!!, -10, this)
        healthSystem!!.tick(entitySystem!!, .1f)

        // ASSERT
        assert(healthBefore + 15 == healthComponent!!.health)
    }

    @Test
    fun healthChangeShouldBeAppliedCorrectlyAfterModifierUnregister() {
        // SETUP
        val healthBefore = healthComponent!!.health

        val modifierA: (Entity, Int, Array<Any>) -> Int = { entity, diff, sources -> -5 }
        val modifierB: (Entity, Int, Array<Any>) -> Int = { entity, diff, sources -> +10 }

        healthSystem!!.registerModifier(entity!!, modifierA)
        healthSystem!!.registerModifier(entity!!, modifierB)

        // ACT
        healthSystem!!.unregisterModifier(entity!!, modifierB)

        healthSystem!!.queueHealthChange(entity!!, -10, this)
        healthSystem!!.tick(entitySystem!!, .1f)

        // ASSERT
        assert(healthBefore - 5 == healthComponent!!.health)
    }

    @Test
    fun onHealthChangeShouldBeCalled() {
        // SETUP
        var causedChange: HealthChange? = null
        var reportedEntity: Entity? = null
        val listener: EntityHealthListener = object : EntityHealthListener {
            override fun onHealthChange(e: Entity, change: HealthChange) {
                reportedEntity = e
                causedChange = change
            }

            override fun onDeath(e: Entity, change: HealthChange) {

            }
        }

        healthSystem!!.registerEntityHealthListener(listener)

        healthSystem!!.queueHealthChange(entity!!, -10, this)

        // ACT
        healthSystem!!.tick(entitySystem!!, .1f)

        // VERIFY
        assert(causedChange   != null,   {"onHealthChange not invoked"})
        assert(reportedEntity != null, {"onHealthChange not invoked"})

        assert(-10 == causedChange!!.difference)
        assert(this == causedChange!!.sources[0])
        assert(reportedEntity === entity!!)
    }

    @Test
    fun onHealthChangeShouldNotBeCalledAfterListenerUnregister() {
        // SETUP
        var called: Boolean = false
        val listener: EntityHealthListener = object : EntityHealthListener {
            override fun onHealthChange(e: Entity, change: HealthChange) {
                called = true
            }

            override fun onDeath(e: Entity, change: HealthChange) {

            }
        }

        // ACT
        healthSystem!!.registerEntityHealthListener(listener)
        healthSystem!!.unregisterEntityHealthListener(listener)

        healthSystem!!.queueHealthChange(entity!!, -10, this)
        healthSystem!!.tick(entitySystem!!, .1f)

        // VERIFY
        assert(!called, {"onHealthChange called when it should not have been"})
    }

    @Test
    fun onDeathShouldBeCalled() {
        // SETUP
        var causedChange: HealthChange? = null
        var reportedEntity: Entity? = null
        val listener: EntityHealthListener = object : EntityHealthListener {
            override fun onHealthChange(e: Entity, change: HealthChange) {

            }

            override fun onDeath(e: Entity, change: HealthChange) {
                reportedEntity = e
                causedChange = change
            }
        }

        healthSystem!!.registerEntityHealthListener(listener)

        healthSystem!!.queueHealthChange(entity!!, -60, this) // entity has 50 health, see @Before method

        // ACT
        healthSystem!!.tick(entitySystem!!, .1f)

        // VERIFY
        assert(causedChange   != null, {"onDeath not called"})
        assert(reportedEntity != null, {"onDeath not called"})

        assert(-60 == causedChange!!.difference)
        assert(this == causedChange!!.sources[0])
        assert(reportedEntity === entity!!)
    }

    @Test
    fun onDeathShouldNotBeCalledAfterListenerUnregister() {
        // SETUP
        var called: Boolean = false
        val listener: EntityHealthListener = object : EntityHealthListener {
            override fun onHealthChange(e: Entity, change: HealthChange) {

            }

            override fun onDeath(e: Entity, change: HealthChange) {
                called = true
            }
        }

        // ACT
        healthSystem!!.registerEntityHealthListener(listener)
        healthSystem!!.unregisterEntityHealthListener(listener)

        healthSystem!!.queueHealthChange(entity!!, -60, this) // entity has 50 health, see @Before method
        healthSystem!!.tick(entitySystem!!, .1f)

        // VERIFY
        assert(!called, {"onDeath called when it should not have been"})
    }
}
